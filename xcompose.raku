
my %xcompose-names = qw{
< less
> greater
| bar
. period
: colon
, comma
; semicolon
- minus
_ underscore
' apostrophe
" quotedbl
` dead_grave
´ dead_acute
+ plus
* asterisk
~ asciitilde
( parenleft
) parenright
[ bracketleft
] bracketright
{ braceleft
} braceright
^ dead_circumflex
° degree
\ backslash
! exclam
§ section
$ dollar
% percent
& ampersand
/ slash
= equal
? question
};

my %xcompose-names-win = qw{
< less
> greater
| bar
. period
: colon
, comma
; semicolon
- minus
_ underscore
' apostrophe
" quotedbl
` `
´ ´
+ plus
* *
~ asciitilde
( parenleft
) parenright
[ [
] ]
{ braceleft
} braceright
^ ^
° degree
\ backslash
! exclam
§ section
$ dollar
% percent
& &
/ slash
= equal
? question
};

%xcompose-names{' '} = 'space';

my @definitions = q:to/EOH/.lines;
<< «
>> »
`` ”
´´ “
"' “
", „
"< «
"> »
'( ‚
') ‘
"( „
") “
"" „“
'' ‘’

<( ⟨
>) ⟩
([ ｢
)] ｣
(L ⌊
)L ⌋
(( ⸨
)) ⸩
?! ‽
?? ⁇
!! ‼
|| ‖

*x ×
/- ÷
<= ≤
=> ≥
!= ≠
:= ≔
=~ ≅
~~ ≈

-> →
<-> ↔

oo ∞
2. ‥
3. …
pi π
tau τ
ee 𝑒

-- —
-. ‐
__ ＿
SHY ­

1/2 ½
1/3 ⅓
1/4 ¼
1/5 ⅕
1/6 ⅙
1/7 ⅐
1/8 ⅛
1/9 ⅑
1/0 ⅒
2/3 ⅔
2/5 ⅖
3/4 ¾
3/5 ⅗
3/8 ⅜
4/5 ⅘
5/6 ⅚
5/8 ⅝
7/8 ⅞

R1 Ⅰ
R2 Ⅱ
R3 Ⅲ
R4 Ⅳ
R5 Ⅴ
R6 Ⅵ
R7 Ⅶ
R8 Ⅷ
R9 Ⅸ
R0 Ⅹ
Rl Ⅼ
Rc Ⅽ
Rd Ⅾ
Rm Ⅿ

^0 ⁰
^1 ¹
^2 ²
^3 ³
^4 ⁴
^5 ⁵
^6 ⁶
^7 ⁷
^8 ⁸
^9 ⁹

() ∅
(e) ∈
!(e) ∉
(c) ∋
!(c) ∌
(==) ≡
!(==) ≢
(<=) ⊆
!(<=) ⊈
(<) ⊂
!(<) ⊄
(>=) ⊇
!(>=) ⊉
(>) ⊃
!(>) ⊅
(|) ∪
(&) ∩
(-) ∖
(^) ⊖
(.) ⊍
(+) ⊎

no ¬
an ∧
or ∨
xo ⊻

fc ∘
+- ±

C0 ␀
Ce ␛
Cn ␤
Cp ¶
Cr ⏎
Cf ␜
Ct ␋
CR ␞
Cs ␠
CT ␄
CB ␈

TM ™
OC ©
OR ®
O0 ⓪
O1 ①
O2 ②
O3 ③
O4 ④
O5 ⑤
O6 ⑥
O7 ⑦
O8 ⑧
O9 ⑨
O10 ⑩
O11 ⑪
O12 ⑫
O13 ⑬
O14 ⑭
O15 ⑮
O16 ⑯
O17 ⑰
O18 ⑱
O19 ⑲
O20 ⑳
O21 ㉑
O22 ㉒
O23 ㉓
O24 ㉔
O25 ㉕
O26 ㉖
O27 ㉗
O28 ㉘
O29 ㉙
O30 ㉚
O31 ㉛
O32 ㉜
O33 ㉝
O34 ㉞
O35 ㉟
O36 ㊱
O37 ㊲
O38 ㊳
O39 ㊴
O40 ㊵
O41 ㊶
O42 ㊷
O43 ㊸
O44 ㊹
O45 ㊺
O46 ㊻
O47 ㊼
O48 ㊽
O49 ㊾
O50 ㊿

atom ⚛
letter ✉
heart ♥
blacksquare ■
square ◻
QED ∎
dead ☠
OK 👍

EOH

multi sub MAIN(IO(Str) :o(:$out-file), :w(:$windows)) {

    %xcompose-names = %xcompose-names-win if $windows;
    $*OUT = $out-file.open(:w) with $out-file;

    for @definitions {
        next if $_ ~~ '';

        .split(' ').map(-> $sequence, $char {
            '<Multi_key> ' ~ $sequence.&xescape ~ ' : "' ~ $char ~ '"'
        }).put;

    }

    put '<Multi_key> <space><space> : " "';
    put '<Multi_key> <space><0> : "​"';
    put '<Multi_key> <C><space> : "␣"';
}

sub xescape(Str $_) {
    .comb.map({ '<' ~ (%xcompose-names{$_} // $_) ~ '>' }).join(' ');
}
